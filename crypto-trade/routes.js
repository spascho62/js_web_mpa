const router = require('express').Router();

const homeController = require('./controllers/homeController');
const authController = require('./controllers/authController');
const cryptoController = require('./controllers/cryptopController');

router.use(homeController);
router.use(authController);
router.use('/crypto', cryptoController);
router.all('*', (req, res) => {
    res.render('home/404')
});
//TODO: Add routes

module.exports = router;