# Cheat Sheet

1. Initialize project
    * podmeni public(papkata) s dannite ot noviq exam
npm init --yes
npm i nodemon -D
npm i express
2. Install and setup Express
    * add routes -> routes.js file
    * add body-parser -> (express.urlencoded({extended: false}))
    * add static routes -> app.use(express.static('public')); - add in public folder css and img files.
3. Add view engine
npm i express-handlebars
    * register with express
    * add views folder
    * add home template - da se podmeni s tezi dadeni na izpita
    * add main layout
    * add partial template folder
4. Add home controller
    * add controller to routes
5. Connect database
npm i mongoose
mongoose.set('strictQuery', false);
6. Authentication
    * fix html links
    * add auth controller
    * add register page
    * add login page
7. Add user model
8. Add auth service
9. Install bcrypt and cookie-parser and config
npm i bcrypt
npm i cookie-parser
10. Register user
    * validate repeat password
    * check if user exists
    * use bcypt to hash password
11. Login user
    * check if user exists
    * check if password is valid
12. Generate jwt
    * npm i jsonwebtoken
    * OPTIONAL: use util.promisify to use async
    * generate token with payload
    * add token to cookie
13. Add authentication middleware
    * add decoded token to request
    * use authentication middleware
14. Logout
15. Authorization middleware
16. Dynamic navigation
17. Error handling (local error handling)
18. Add error notification to main layout
19. Login automatically after register
20. Parse errors

---